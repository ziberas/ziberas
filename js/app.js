const us = document.getElementById("usuario")
const nombre = document.getElementById("nombrec")
const email = document.getElementById("correo")
const pass = document.getElementById("contrasena")
const form = document.getElementById("login-form text-center")
const parrafo = document.getElementById("warnings")

form.addEventListener("submit", e=>{
    e.preventDefault()
    let warnings = ""
    let entrar = false
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/
    parrafo.innerHTML = ""
    if (us.value.length < 6) {
        warnings += `El usuario no es valido <br>`
        entrar = true
    }
    if (nombre.value.length < 9) {
        warnings += `El nombre no es valido <br>`
        entrar = true
    }
    if(!regexEmail.test(email.value)){
        warnings += `El email no es valido <br>`
        entrar = true
    }
    if(pass.value.length < 8){
        warnings += `La contraseña no es valida <br>`
        entrar = true
    }

    if(entrar){
        parrafo.innerHTML = warnings
    }else{
        parrafo.innerHTML = "Enviado"
    }
})