﻿<?php

$alert = '';
session_start();
if(!empty($_SESSION['active']))
{
    header('location: sistema/');
}
else {

    if(!empty($_POST)){
    if(empty($_POST['usuario'])|| empty($_POST['clave']))
    {
        $alert = 'Inglese su usuario y su clave';
    }
    else{
        require_once "conexion.php";
        $user = $_POST['usuario'];
        $pass = $_POST['clave'];

        $query = mysqli_query($conection,"SELECT * FROM usuario WHERE usuario = '$user' AND clave = '$pass'");
        $result = mysqli_num_rows ($query);
        
        if($result>0){
            $data = mysqli_fetch_array ($query);
            $_SESSION['active']= true;
            $_SESSION['idUser']= $data['idusuario'];
            $_SESSION['nombre']= $data['nombre'];
            $_SESSION['email']= $data['email'];
            $_SESSION['user']= $data['usuario'];
            $_SESSION['rol']= $data['rol'];

            header('location: sistema/');
        }
        else{
            $alert = 'El usuario o la clave son incorrectas';
            session_destroy();
        }

    }
}
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
    <title>Bootstrap Login Form</title>
</head>

<body>

    <div class="d-flex justify-content-center align-items-center login-container">
        <form method="POST"  action="" class="login-form text-center">
            <h1 class="mb-5 font-weight-light text-uppercase">Iniciar Sesión</h1>
            <div class="form-group">
                <input type="text" class="form-control rounded-pill form-control-lg" placeholder="Usuario" name="usuario">
            </div>
            <div class="form-group">
                <input type="password" class="form-control rounded-pill form-control-lg" placeholder="Contraseña" name="clave">
            </div>
            <div class="forgot-link form-group d-flex justify-content-between align-items-center">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="remember">
                    <label class="form-check-label" for="remember">Recordar Contraseña</label>
                </div>
                <a href="#">¿Se te olvidó tu contraseña?</a>
            </div>
            <div class="alert"><?php echo isset($alert)? $alert:'';?></div>
            <button type="submit" class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase">Iniciar</button>
            <p class="mt-3 font-weight-normal">¿No tienes una cuenta? <a href="registro.html"><strong>Registrar Ahora</strong></a></p>
        </form>
    </div>

    <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>