 <!-- Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&amp;subset=latin-ext"
          rel="stylesheet">
    <?php "functions.php"; ?>
    <!-- CSS - REQUIRED - START -->
    <!-- Batch Icons -->
    <link rel="stylesheet" href="assets/fonts/batch-icons/css/batch-icons.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap/mdb.min.css">
    <!-- Custom Scrollbar -->
    <link rel="stylesheet" href="assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Hamburger Menu -->
    <link rel="stylesheet" href="assets/css/hamburgers/hamburgers.css">

    <!-- CSS - REQUIRED - END -->

    <!-- CSS - OPTIONAL - START -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
    <!-- JVMaps -->
    <link rel="stylesheet" href="assets/plugins/jvmaps/jqvmap.min.css">
    <!-- CSS - OPTIONAL - END -->

    <!-- QuillPro Styles -->
    <link rel="stylesheet" href="assets/css/quillpro/quillpro.css">

<!-- SCRIPTS - REQUIRED START -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Bootstrap core JavaScript -->
<!-- JQuery -->
<script type="text/javascript" src="assets/js/jquery/jquery-3.1.1.min.js"></script>
<!-- Popper.js - Bootstrap tooltips -->
<script type="text/javascript" src="assets/js/bootstrap/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="assets/js/bootstrap/mdb.min.js"></script>
<!-- Velocity -->
<script type="text/javascript" src="assets/plugins/velocity/velocity.min.js"></script>
<script type="text/javascript" src="assets/plugins/velocity/velocity.ui.min.js"></script>
<!-- Custom Scrollbar -->
<script type="text/javascript" src="assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- jQuery Visible -->
<script type="text/javascript" src="assets/plugins/jquery_visible/jquery.visible.min.js"></script>
<!-- jQuery Visible -->
<script type="text/javascript" src="assets/plugins/jquery_visible/jquery.visible.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="assets/js/misc/ie10-viewport-bug-workaround.js"></script>

<!-- SCRIPTS - REQUIRED END -->

<!-- SCRIPTS - OPTIONAL START -->
<!-- ChartJS -->
<script type="text/javascript" src="assets/plugins/chartjs/chart.bundle.min.js"></script>
<!-- JVMaps -->
<script type="text/javascript" src="assets/plugins/jvmaps/jquery.vmap.min.js"></script>
<script type="text/javascript" src="assets/plugins/jvmaps/maps/jquery.vmap.usa.js"></script>
<!-- Image Placeholder -->
<script type="text/javascript" src="assets/js/misc/holder.min.js"></script>
<!-- SCRIPTS - OPTIONAL END -->

<!-- QuillPro Scripts -->
<script type="text/javascript" src="assets/js/scripts.js"></script>
<script type="text/javascript" src="assets/js/i7system.js"></script>
<!-- Script Josue-->
<script type="text/javascript" src="assets/js/functions.js"></script>